#!/usr/bin/env python2
# coding=utf8

"""Module with a thread which runs audio detection and labeling, e.g. checking with which word the recorded sample
   corresponds.
   .. module: labeling_thread
   :author: Elias Deking
"""

from PyQt4.QtCore import QThread, pyqtSignal
from auditok import ADSFactory, AudioEnergyValidator, StreamTokenizer

import label_audio


class LabelingThread(QThread):
    error_message = pyqtSignal(object)
    got_label = pyqtSignal(object, object)  # Signal which transmits the label and the recorded data to the GUI
    recorded_word = pyqtSignal(object)  # Signal which is called whenever something is recorded
    energy_threshold = 65

    def __init__(self, mw, graph_definition, labels):
        super(LabelingThread, self).__init__(mw)  # Initialize the thread with the MainWindow as its parent
        self.mw = mw
        self.graph_definition = graph_definition
        self.labels = labels

    def run(self):
        """Function which is used to record audio and label it using the trained TensorFlow model.
       Adapted from the speech_recorder script of this project.
        """
        while True:
            asource = ADSFactory.ads(record=True, max_time=1, sampling_rate=16000)
            validator = AudioEnergyValidator(sample_width=asource.get_sample_width(),
                                             energy_threshold=self.energy_threshold)
            tokenizer = StreamTokenizer(validator=validator, min_length=50, max_length=1000, max_continuous_silence=30)
            asource.open()  # open the audio stream
            recording = tokenizer.tokenize(asource)  # tokenize it (e.g. split it up into single words)
            asource.close()  # close it again
            if len(recording) > 0:
                data = b''.join(recording[0][0])
                # Add a valid RIFF WAVE header to the recorded data so that TensorFlow recognises it
                data_with_header = self.add_wav_header(data)
                self.get_label(data_with_header)

    def get_label(self, wav_data):
        """Function which creates a object of the LabelAudio class which is used to label the audio data.
        """
        # Create a labeling instance of the class LabelAudio
        labeling_instance = label_audio.LabelAudio(self.graph_definition, self.labels)
        results = labeling_instance.label_word(wav_data)
        self.got_label.emit(results[0], wav_data)

    def add_wav_header(self, raw_data):
        """Function which is used to add a valid RIFF WAVE header to the raw audio data so that TensorFlow accepts
        it as a valid input.
        The header was read from a generated and valid .wav-file and is valid for the parameters:
        Channels: 1 (Mono)
        Framerate: 16000
        Sampling width: 2
        """
        import binascii
        header = binascii.unhexlify('52494646')
        import struct
        header += struct.pack('<L', len(raw_data) + 36)
        header += binascii.unhexlify('57415645666d74201000000001000100803e0000007d00000200100064617461')
        header += struct.pack('<L', len(raw_data))
        return header + raw_data
