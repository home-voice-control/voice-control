voice-control
=============

.. toctree::
   :maxdepth: 4

   about
   animated_label
   audio_visualisation
   icons_rc
   label_audio
   labeling_thread
   main
   mainwindow
