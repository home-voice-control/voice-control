voice-control
=============

.. toctree::
   :maxdepth: 4

   animated_label
   audio_visualisation
   label_audio
   labeling_thread
   main
