.. voice-control documentation master file, created by
   sphinx-quickstart on Sun Oct 28 21:52:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for the project "voice-control"
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. include:: modules.rst
