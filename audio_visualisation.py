from auditok import AudioEnergyValidator
import numpy as np
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas


class AudioVisualisation(FigureCanvas):
    """
    Class which represents a Matplotlib plotting widget which can be embedded in a Qt application.
    It is used to display the recorded waveform data as a visual feedback of the recording.
    The main code is adapted from the book "Matplotlib for Python Developers", Sandro Tosi, Packt Publishing, 2009
    and from the auditok functions _plot and plot_all.
    """
    def __init__(self, parent):
        self.fig = Figure()  # Create a Figure object on which the plot will be drawn
        # Add an axes object as a subplot, which is in the first row, first column and first position (111)
        self.axes = self.fig.add_subplot(111)
        # Color settings
        self.fig.set_facecolor('#323232')
        self.axes.set_facecolor('#323232')
        self.axes.tick_params('both', color='#b1b1b1', labelcolor='#b1b1b1')
        for spine in self.axes.spines.values():
            spine.set_color('#b1b1b1')
        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

    def plot_data(self, data, sample_width, sampling_rate):
        # convert audio data to energy levels, e.g. able to be shown as a waveform
        signal = AudioEnergyValidator._convert(data, sample_width)
        signal /= np.max(np.abs(signal), axis=0)  # normalize it to -1;1, based on: https://stackoverflow.com/a/1735122
        t = np.arange(0., np.ceil(float(len(signal))) / sampling_rate,
                      1. / sampling_rate)  # generate x-Axis data from the number of samples and the sampling_rate
        if len(t) > len(signal):
            t = t[: len(signal) - len(t)]  # Make sure the x-Axis isn't longer than the amount of data supplied
        self.axes.plot(t, signal, color='xkcd:blue')  # Plot the signal over time
        self.draw()  # and display it
        self.axes.clear()  # Clear the axes for next time
