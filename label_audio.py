#!/usr/bin/env python2
# coding=utf8

"""A module which performs the task of labeling audio data using the trained model.
.. module:: label_audio
   :author: Elias Deking
"""
import tensorflow as tf


class LabelAudio(object):
    """Class that calculates the probabilities of each label corresponding to given audio data using a TensorFlow model.
    """

    def __init__(self, graph_def, labels):
        """Initializes the class.
        
        :param graph_def: Definition of the TensorFlow Graph.
        :type graph_def: tf.GraphDef
        :param labels: Array of strings containing one label per entry.
        :type labels: [str]
        """
        tf.import_graph_def(graph_def, name='')
        self.labels = labels

    def label_word(self, wav_data):
        """Takes audio data as an input and returns the probabilities of each label corresponding with the data.
        
        :param wav_data: The audio data in `.wav` format.
        :type wav_data: str
        :returns: [(str, float)] -- an array of tuples which contain the name of the label and its corresponding probability
        """
        with tf.Session() as sess:
            tensor = sess.graph.get_tensor_by_name('labels_softmax:0')  # get the first tensor in the graph
            probabilities, = sess.run(tensor, {'wav_data:0': wav_data})
            # Sort list to let the labels with the highest probability be shown first
            sorted_probabilities = probabilities.argsort()[::-1]
            return_values = []
            for id in sorted_probabilities:
                return_values.append((self.labels[id], probabilities[id]))
            return return_values
