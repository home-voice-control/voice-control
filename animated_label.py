#!/usr/bin/env python2
# coding=utf8

"""
    Module which contains the AnimatedLabel class.
    .. module: animated_label
   :author: Elias Deking
"""

from PyQt4 import QtGui, QtCore


class AnimatedLabel(QtGui.QWidget):
    """
    Class which inherits from QWidget and contains a label whose text can be edited and faded in and out.
    """
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.label = QtGui.QLabel(self)  # Create the label which displays the text
        self.label.setGeometry(QtCore.QRect(0, 0, 273, 51))
        self.label.setAlignment(QtCore.Qt.AlignCenter)  # Align the text so that it is centered
        self.label.setStyleSheet("background-color: rgba(255,255,255,0)")  # Set a transparent background
        # Font settings
        self.font = QtGui.QFont()
        self.font.setFamily("Ubuntu")
        self.font.setPointSize(36)
        self.font.setItalic(False)
        self.label.setFont(self.font)
        # Create QTimeLine object which calls the function fade_out 25 times per second to update the font's opacity
        self.fade_time_line = QtCore.QTimeLine()
        self.fade_time_line.valueChanged.connect(self.fade_out)
        self.fade_time_line.setDuration(500)  # The duration of the fadeout in milliseconds

    def show_text(self, text, duration):
        """Set the text of the inner label and display it for a given amount of seconds.
        :param text: The text which is displayed.
        :param duration: The amount of time it is displayed for in seconds, after which it fades out.
        """
        self.label.setText(text)  # Set the text of the label
        # Reset the text back to a white color and make the background transparent white again
        self.label.setStyleSheet("color: rgba(255,255,255,255); background-color: rgba(255,255,255,50)")
        # Wait for the time specified by 'duration' and start fading out after the time has passed
        QtCore.QTimer.singleShot(duration * 1000.0, self.fade_time_line.start)

    def fade_out(self, current_value):
        """The function which is called by the QTimeLine object. It sets the opacity (alpha channel) of the font
        according to the input value.
        :param current_value: Input value which ranges between 0.0 - 1.0 and is inversely mapped to values 255 - 0.
        """
        # Calculate the alpha value which is determined by the input parameter current_value
        text_alpha = (1.0-current_value) * 255.0
        background_alpha = (1.0-current_value) * 50.0
        self.label.setStyleSheet("color: rgba(255,255,255,{:03.0f});".format(text_alpha) +
                                 "background-color: rgba(255,255,255,{:03.0f})".format(background_alpha))
