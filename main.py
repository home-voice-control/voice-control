#!/usr/bin/env python2
# coding=utf8

"""Main module which runs the program.
   .. module:: main
   :author: Elias Deking
"""
import sys

import pyautogui
import tensorflow as tf
from PyQt4 import QtGui, QtCore

from PyQt4.QtGui import QMessageBox

from labeling_thread import LabelingThread
from mainwindow import Ui_MainWindow
from about import Ui_Dialog


class Gui(QtGui.QMainWindow):
    """
    Class which initializes the GUI in mainwindow.py. This GUI was created and can be edited with Qt Designer,
    using the mainwindow.ui file found under gui_files/. After being edited, this file has to be compiled via the
    build_ui.py script, which is also found under gui_files/.
    """
    graph_path = "test_files/conv_actions_frozen.pb"
    label_path = "test_files/conv_actions_labels.txt"

    def __init__(self, parent=None):
        # Call the constructor of QMainWindow and signal the window manager to make the window stay on top
        super(Gui, self).__init__(parent, QtCore.Qt.WindowStaysOnTopHint)
        self.ui = Ui_MainWindow()  # Initialize a form which contains the UI (created in Qt Designer + build_ui.py)
        self.ui.setupUi(self)  # And set it up (show all buttons, labels etc.)

        # Create a flag which signifies if the recorded words should be output as keystrokes
        self.output_keystrokes = False

        # Import graph from the set path
        graph_definition = tf.GraphDef()  # Construct a variable which is able to contain the TF graph definition
        self.load_graph(self.graph_path, graph_definition)  # Fill this variable with the graph model
        labels = self.load_labels(self.label_path)

        self.label_thread = LabelingThread(self, graph_definition, labels)
        self.label_thread.start()
        self.label_thread.error_message.connect(self.display_error_message)
        self.label_thread.got_label.connect(self.got_label)

        # Connect ui elements
        self.ui.sensitivitySlider.valueChanged.connect(self.sensitivity_changed)
        self.ui.recordSwitch.clicked.connect(self.toggle_record)
        self.ui.action_About.triggered.connect(self.show_about_dialog)
        self.ui.action_Close.triggered.connect(sys.exit)

        # Map possible labels to key identifiers for PyAutoGUI and images which are shown on the right
        # Structure: key : [key_identifier_for_pyautogui, link_to_image]
        # The key identifiers are available at: https://pyautogui.readthedocs.io/en/latest/keyboard.html#keyboard-keys
        self.map_labels_to_outputs = {
            "left":  ["left",  ":/icons/arrows_left.png"],
            "right": ["right", ":/icons/arrows_right.png"],
            "up":    ["up",    ":/icons/arrows_up.png"],
            "down":  ["down",  ":/icons/arrows_down.png"]
        }

        # Show "about" dialog on startup
        self.show_about_dialog()

    def load_graph(self, path, graph_definition):
        """Function which tries to import and load a graph from a given path."""
        if tf.gfile.Exists(path):
            with tf.gfile.FastGFile(path, 'rb') as graph_file:
                graph_definition.ParseFromString(graph_file.read())
        else:
            raise IOError("The specified TensorFlow graph file does not exist!")

    def load_labels(self, path):
        """
        Function which tries to import and parse the labels into a string array from a given text file.
        :param path: str - The path to the file.
        :return: [str] - A 1-dimensional array of strings containing the labels.
        """
        with open(path, 'r') as label_file:
            return label_file.read().split('\n')

    # GUI functions
    def display_error_message(self, error_string):
        """
        Function which displays an error dialog with the button "OK" and the error itself.
        :param error_string: str - The string that will be displayed as the error message.
        """
        message = QMessageBox()
        message.setIcon(QMessageBox.Critical)
        message.setText(error_string)
        message.setWindowTitle("An error has occured")
        message.setStandardButtons(QMessageBox.Ok)
        message.exec_()

    def got_label(self, label, wav_data):
        """
        Function which is executed when a label has been recorded and identified.
        It switches the keystroke output switch on and off, depending on what is said
        and shows the recognized label on top of the graph in the middle of the UI.
        Additionally, it sends out keystrokes if the switch is turned on and updates the graph with the new audio data.
        :param label: str - The recognized label of the audio recording by TensorFlow
        :param wav_data: str - The recorded WAV data with a valid RIFF WAVE header
        """
        # Remove underscores and capitalize the first letter
        text_to_show = label[0].replace('_', '').capitalize()
        self.ui.label.show_text(text_to_show, 0.5)  # Show the specified text for 0.5 seconds
        if label[0] in ("on", "go"):
            self.output_keystrokes = True
            self.ui.recordSwitch.setChecked(True)
        elif label[0] in ("off", "stop"):
            self.output_keystrokes = False
            self.ui.recordSwitch.setChecked(False)

        # Output keystrokes if the boolean flag is set to True and the label corresponds with one of the arrow keys
        if self.output_keystrokes and (label[0] in self.map_labels_to_outputs.keys()):
            # Get the corresponding entry in the mapping dictionary
            current_outputs = self.map_labels_to_outputs[label[0]]
            # Press the key which corresponds to the mapped one in maps_labels_to_outputs (see __init__())
            pyautogui.press(current_outputs[0])
            # Set the image on the right to show the pressed key for 1000 ms and reset it thereafter
            self.ui.icon.setPixmap(QtGui.QPixmap(current_outputs[1]))  # Set the image
            QtCore.QTimer.singleShot(1000.0, self.reset_icon)

        # Remove the RIFF WAVE header (first 44 bytes) and plot the data
        self.ui.widget.plot_data(wav_data[44:], 2, 16000)

    def sensitivity_changed(self):
        """
        Function which is executed when the sensitivity slider's value has changed.
        It gets the value and sets it in the labeling thread.
        """
        sensitivity = self.ui.sensitivitySlider.value()
        self.label_thread.energy_threshold = 100-sensitivity
        self.ui.sensitivityLabel.setText("<center>Sensitivity<br>" + str(sensitivity)+"</center>")

    def toggle_record(self):
        """
        Function which is executed whenever the record button is toggled or "on", "off", "stop" or "go" is said.
        It checks the current state of the button and sets the boolean flag accordingly.
        """
        if self.ui.recordSwitch.isChecked():
            self.output_keystrokes = True
        else:
            self.output_keystrokes = False

    def reset_icon(self):
        """
        Function which resets the image on the right to the standard image where no key is displayed as pressed.
        """
        self.ui.icon.setPixmap(QtGui.QPixmap(":/icons/arrows_none.png"))

    def show_about_dialog(self):
        """
        Function which creates a new dialog with UI imported from about.py in it.
        about.py was generated using Qt Designer and pyuic from gui_files/about.ui, just like the main window.
        """
        about = QtGui.QDialog()
        about.ui = Ui_Dialog()
        about.ui.setupUi(about)
        about.exec_()


if __name__ == '__main__':
    app = QtGui.QApplication([])
    window = Gui()
    window.show()
    sys.exit(app.exec_())
